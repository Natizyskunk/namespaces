<?php
    declare(strict_types = 1);

    require 'Classes\MyClass.php';
    
    use One\MyClass;
    
    $MyClass = new MyClass();

    echo "---\n";
    echo $MyClass->busIntro() . "\n" ;
    echo $MyClass->myPrivateCall();
?>
