<?php
    declare(strict_types = 1);
    
    namespace One;
    
    class MyClass
    {
        private $string = '';

        public function __construct()
        {
            $this->busIntro();
            $this->myPrivateCall();
        }

        private function myPrivate(): string {
            return "I'm private !";
        }

        public function busIntro(): string
        {
            $this->string = "Hello World !";
            $testing = $this->string;

            return $testing;
        }

        public function myPrivateCall(): string {
            return $this->myPrivate();
        }
    }

?>
